import 'package:flutter/material.dart';
import 'package:house_rent/screen/home/home.dart';
import 'package:house_rent/screen/home/widget/landing_screen.dart';
import 'package:house_rent/screen/home/widget/tabbar.dart';
import 'package:house_rent/screen/home/widget/youtube_ui.dart';

import 'package:house_rent/screen/main_page/main_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(Object context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            useMaterial3: true,
            backgroundColor: Color(0xFFF5F6F6),
            primaryColor: Color(0xFF811B83),
            accentColor: Color(0xFFFA5019),
            textTheme: TextTheme(
              headline1: TextStyle(
                color: Color(0xFF100E34),
              ),
              bodyText1: TextStyle(color: Color(0xFF100E34).withOpacity(0.5)),
            )),
         home: LandingScreen());
        //home: HomePage());

  }
}
