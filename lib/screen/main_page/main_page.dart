import 'package:flutter/material.dart';
import 'package:house_rent/screen/home/widget/nav_example.dart';
import 'package:house_rent/screen/home/widget/notification.dart';
import 'package:house_rent/screen/profile/main_profile.dart';
import '../home/widget/cart_page.dart';
import '../home/widget/tabbar.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentPageIndex = 0;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var pages = [
    TabBarProduct(),
    Cart(),
    FavoritePage(),
    MainProfile()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        key: scaffoldKey,
        bottomNavigationBar: NavigationBarTheme(
          data: NavigationBarThemeData(
            indicatorColor: Colors.blue[200],
            // surfaceTintColor: Colors.white
          ),
          child: NavigationBar(
            backgroundColor: Colors.transparent,
            //surfaceTintColor: Colors.white,
            animationDuration: Duration(seconds: 1),
            onDestinationSelected: (int index) {
              setState(() {
                currentPageIndex = index;
              });
            },
            selectedIndex: currentPageIndex,
            destinations:  <Widget>[
              NavigationDestination(
                icon: const Icon(Icons.home),
                label: 'Home',
              ),
              // const NavigationDestination(
              //   icon: Icon(Icons.people,),
              //   label: 'Company',
              // ),
              const NavigationDestination(
                icon: Icon(Icons.shopping_cart),
                label: 'Cart',
              ),
              const NavigationDestination(
                // selectedIcon: Icon(Icons.bookmark),
                icon: Icon(Icons.favorite),
                label: 'Favorite',
              ),
              const NavigationDestination(
                // selectedIcon: Icon(Icons.bookmark),
                icon: Icon(Icons.person_outlined),
                label: 'profile',
              ),
            ],
          ),
        ),
        body: IndexedStack(
          index: currentPageIndex,
          children: pages,
        )
    );
  }
}
