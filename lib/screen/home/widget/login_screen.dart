import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:house_rent/screen/home/widget/signup_screen.dart';

class LoginScreen extends StatelessWidget {
  //static const routeName = '/loginScreen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BackButtonLS(),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Log In Continue!',
                            style: TextStyle(fontSize: 25,fontWeight: FontWeight.w700,color: Colors.black)
                          ),
                        ],
                      ),
                      Spacer(),
                      SocialMediaLogin(
                        method: 'Login',
                      ),
                      Spacer(),
                      OrRow(),
                      Spacer(),
                      TextFields(),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Forgot Password?',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Container(
                        width: double.infinity,
                        height: 60,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.red, // Background color
                          ),
                          onPressed: () {
                            //Navigator.of(context).pushNamed(TabScreen.routeName);
                          },
                          child: Text('Login',style: TextStyle(fontSize: 18,color: Colors.white),),
                        ),
                      ),
                      Spacer(
                        flex: 4,
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>SignupScreen()));
                        },
                        child: OptionButton(
                          desc: 'Don\'t have an account? ',
                          method: 'Sign Up',
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class TextFields extends StatelessWidget {
  const TextFields({
     Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextField(
          hint: 'Email Address', a: false,
        ),
        SizedBox(
          height: 16,
        ),
        CustomTextField(
          a: false,
          hint: 'Password',
          icon: Image.asset('assets/images/hide_icon.png'),
        ),
      ],
    );
  }
}

class BackButtonLS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return RawMaterialButton(
      child: Icon(
        Icons.arrow_back_ios,
        color: Colors.black,
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
      constraints: BoxConstraints.tightFor(
        width: 32,
        height: 40,
      ),
    );
  }
}


class CustomTextField extends StatelessWidget {
  const CustomTextField({

    required this.hint,
   this.icon, this.controller,
    required this.a, this.enableInteractiveSelection,
  });
  final bool a;
  final TextEditingController? controller;
  final String hint;
  final Widget? icon;

  final bool? enableInteractiveSelection;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: a,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            8,
          ),
          borderSide: BorderSide(
            color:Colors.black,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            8,
          ),
          borderSide: BorderSide(
            color: Colors.black,
          ),
        ),
        hintText: hint,
        suffixIcon: icon,
      ),
    );
  }
}


class OptionButton extends StatelessWidget {
  final String ?desc;
  final String ?method;
  //final Function? onPressHandler;

  const OptionButton({this.desc, this.method,});
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: Theme.of(context).textTheme.bodyText2,
        children: [
          TextSpan(text: desc),
          TextSpan(
            text: method,
            style: Theme.of(context).textTheme.bodyText2?.copyWith(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
class OrRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              SizedBox(
                height: 8,
              ),
              Divider(),
              SizedBox(
                height: 8,
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal:16,
          ),
          child: Text(
            'OR',
            style: Theme.of(context)
                .textTheme
                .headline4
                ?.copyWith(color: Colors.grey),
          ),
        ),
        Expanded(
          child: Column(
            children: [
              SizedBox(
                height: 8,
              ),
              Divider(),
              SizedBox(
                height: 8,
              )
            ],
          ),
        ),
      ],
    );
  }
}

class SocialMediaLogin extends StatelessWidget {
  const SocialMediaLogin({
    this.method,
  });

  final String? method;

  @override
  Widget build(BuildContext context) {
    return ElevatedButtonTheme(
      data: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.red.shade400,
          ),
          foregroundColor: MaterialStateProperty.all(
            Colors.red.shade400,
          ),
          elevation: MaterialStateProperty.all(
            0,
          ),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                4,
              ),
            ),
          ),
          alignment: Alignment.centerLeft,
          textStyle: MaterialStateProperty.all(
            TextStyle(
              fontSize: 16,
            ),
          ),
          minimumSize: MaterialStateProperty.all(
            Size(
              double.infinity,
              56,
            ),
          ),
        ),
      ),
      child: Column(
        children: [
          ElevatedButton(
            onPressed: () {},
            child: Text(
              '$method with Facebook',style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text(
              '$method with Google',style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}