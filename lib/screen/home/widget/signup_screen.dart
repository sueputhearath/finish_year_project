import 'package:flutter/material.dart';
import 'package:house_rent/screen/home/widget/order_screen.dart';
import 'package:house_rent/screen/main_page/main_page.dart';
import 'login_screen.dart';

class SignupScreen extends StatelessWidget {
 static const routeName = '/signupScreen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BackButtonLS(),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Sign Up Continue!',
                          style: TextStyle(fontSize: 23,fontWeight: FontWeight.w700)
                        ),
                      ],
                    ),
                    Spacer(),
                    SocialMediaLogin(
                      method: 'Sign Up',
                    ),
                    Spacer(),
                    OrRow(),
                    Spacer(),
                    CustomTextField(
                      a: false,
                      hint: 'Your Name',
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: CustomTextField(
                            a: true,
                           // readOnly: true,
                            //enable: false,
                            hint: '+885',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              size: 24,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 5,
                          child: CustomTextField(
                            a: false,
                            hint: 'Phone Number',
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    CustomTextField(
                        a: false,
                        hint: 'Email Address'),
                    Spacer(),
                    CustomTextField(
                      a: false,
                      hint: 'Password',
                      icon: Image.asset('assets/images/hide_icon.png'),
                    ),
                    Spacer(),
                    Container(
                      height: 60,
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red, // Background color
                          onPrimary: Colors.white, // Text Color (Foreground color)
                        ),
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>OrderScreen()));
                        },
                        child: Text('Sign Up',style: TextStyle(fontSize: 18),),
                      ),
                    ),
                    Spacer(),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                      },
                      child: OptionButton(
                        desc: 'Have an account? ',
                        method: 'Login',
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
